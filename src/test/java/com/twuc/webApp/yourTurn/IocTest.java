package com.twuc.webApp.yourTurn;

import com.twuc.webApp.entity.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;

import static org.junit.jupiter.api.Assertions.*;

class IocTest {

    private AnnotationConfigApplicationContext context;

    @BeforeEach
    private void initTestContext() {
        context = new AnnotationConfigApplicationContext("com.twuc.webApp");
    }

    @Test
    void should_get_the_same_instance_when_implement() {
        InterfaceOne bean = context.getBean(InterfaceOne.class);
        InterfaceOneImpl bean1 = context.getBean(InterfaceOneImpl.class);
        assertSame(bean, bean1);
    }

    @Test
    void should_get_the_same_instance_when_extends() {

        SuperClass superClass = context.getBean(SuperClass.class);
        SonClass sonClass = context.getBean(SonClass.class);
        assertSame(sonClass, superClass);
    }

    @Test
    void should_get_the_same_instance_when_extend_abstract() {

        AbstractBaseClass bean1 = context.getBean(AbstractBaseClass.class);
        DerivedClass bean2 = context.getBean(DerivedClass.class);
        assertSame(bean1, bean2);

    }

    @Test
    void should_get_different_instance_when_get_prototype_bean() {
        SimplePrototypeScopeClass bean = context.getBean(SimplePrototypeScopeClass.class);
        SimplePrototypeScopeClass bean1 = context.getBean(SimplePrototypeScopeClass.class);
        assertNotSame(bean, bean1);
    }

    @Test
    void should_create_bean_when_scan_scope() {
        MyLogger bean = context.getBean(MyLogger.class);
        assertEquals("create the bean.", bean.getLogger().get(0));
    }

    @Test
    void should_create_prototype_bean_when_get_bean() {
        MyLogger bean = context.getBean(MyLogger.class);
        assertEquals(1, bean.getLogger().size());
        context.getBean(SimplePrototypeScopeClass.class);
        assertEquals(2, bean.getLogger().size());
    }

    @Test
    void should_lazy_load_singleton_bean() {
        MyLogger bean = context.getBean(MyLogger.class);
        assertEquals(1, bean.getLogger().size());
        LazyObject bean1 = context.getBean(LazyObject.class);
        LazyObject bean2 = context.getBean(LazyObject.class);
        assertSame(bean1, bean2);
        assertEquals(2, bean.getLogger().size());
    }

    @Test
    void should_create_one_singleton_and_prototype_when_dependent() {

        SingletonDependsOnPrototype bean = context.getBean(SingletonDependsOnPrototype.class);
        SingletonDependsOnPrototype bean1 = context.getBean(SingletonDependsOnPrototype.class);
        assertSame(bean, bean1);
        assertSame(bean.getPrototypeDependent(), bean1.getPrototypeDependent());
    }

    @Test
    void should_create_one_singleton_and_create_two_prototype() {
        PrototypeScopeDependsOnSingleton pBean = context.getBean(PrototypeScopeDependsOnSingleton.class);
        PrototypeScopeDependsOnSingleton pBean1 = context.getBean(PrototypeScopeDependsOnSingleton.class);
        assertSame(pBean.getSingletonDependent(), pBean1.getSingletonDependent());
        assertNotSame(pBean, pBean1);
    }

    @Test
    void should_create_different_prototype_instance() {
        MyLogger myLogger = context.getBean(MyLogger.class);
        assertEquals(1, myLogger.getLogger().size());

        SingletonDependsOnPrototypeProxy bean = context.getBean(SingletonDependsOnPrototypeProxy.class);
        bean.someMethod();
        assertEquals(2, myLogger.getLogger().size());
        SingletonDependsOnPrototypeProxy bean1 = context.getBean(SingletonDependsOnPrototypeProxy.class);
        bean1.someMethod();
        assertEquals(3, myLogger.getLogger().size());
    }

    @Test
    void should_create_different_prototype_instance_when_call_to_string() {
        MyLogger myLogger = context.getBean(MyLogger.class);
        assertEquals(1, myLogger.getLogger().size());

        SingletonDependsOnPrototypeProxy bean = context.getBean(SingletonDependsOnPrototypeProxy.class);
        bean.getPrototypeDependentWithProxy().toString();
        assertEquals(2, myLogger.getLogger().size());
        SingletonDependsOnPrototypeProxy bean1 = context.getBean(SingletonDependsOnPrototypeProxy.class);
        bean.getPrototypeDependentWithProxy().toString();
        assertEquals(3, myLogger.getLogger().size());
    }

    @Test
    void should_create_the_prototype_() {


        SingletonDependsOnPrototypeProxy bean = context.getBean(SingletonDependsOnPrototypeProxy.class);
        System.out.println(bean.getClass().getSuperclass().getName());
        assertNotEquals("com.twuc.webApp.entity.PrototypeDependentWithProxy", bean.getPrototypeDependentWithProxy().getClass().getName());

    }

}
