package com.twuc.webApp.entity;


import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

@Component
public class InterfaceOneImpl implements InterfaceOne {

    private MyLogger myLogger;

    public InterfaceOneImpl(MyLogger myLogger) {
        this.myLogger = myLogger;
        this.myLogger.getLogger().add("create the bean.");
    }

    public MyLogger getMyLogger() {
        return myLogger;
    }
}
