package com.twuc.webApp.entity;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

@Component
@Lazy
public class LazyObject {

    private MyLogger myLogger;

    public LazyObject(MyLogger myLogger) {
        this.myLogger = myLogger;
        this.myLogger.getLogger().add("create the bean.");
    }

    public MyLogger getMyLogger() {
        return myLogger;
    }
}
