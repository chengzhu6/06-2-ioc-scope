package com.twuc.webApp.entity;

import org.springframework.stereotype.Component;

@Component
public class SingletonDependsOnPrototype {

    private PrototypeDependent prototypeDependent;

    public SingletonDependsOnPrototype(PrototypeDependent prototypeDependent) {
        this.prototypeDependent = prototypeDependent;
    }

    public PrototypeDependent getPrototypeDependent() {
        return prototypeDependent;
    }
}
